import unicodecsv
import json
import time
import sys
from time import sleep
from elasticsearch import Elasticsearch
from elasticsearch import helpers

INDEX_NAME = "sf_movie_locations"
DOC_TYPE = "movie_loc"
INDEX_SCHEMA = '''
{
  "mappings": {
    "movie_loc": {
      "properties": {
        "release_year": {
          "type": "integer",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        }
      }
    }
  },
  "settings": {
    "index": {
      "number_of_shards": "1",
      "number_of_replicas": "0"
    }
  }
}
'''

def bulkLoadLocations(es):
  print("Indexing locations")
  csvfile = open('data/Film_Locations_in_San_Francisco.csv', 'r')
  fieldnames = ("title","release_year","locations","fun_facts","production_company",
  "distributor","director","writer","actor_1","actor_2","actor_3")
  reader = unicodecsv.DictReader(csvfile, fieldnames)
  next(reader) # skips first row (header)
  docs = [
    {
      "_index": INDEX_NAME,
      "_type": DOC_TYPE,
      "_source": row
    }
    for row in reader
  ]
  docsIndexed = helpers.bulk(es, docs)[0]
  print(str(docsIndexed) + " " + DOC_TYPE + " docs indexed to " + INDEX_NAME)

def main():
  es = Elasticsearch([{'host': 'elasticsearch'}])

  for x in range(0, 10):  # try 10 times
    try:
        print("ES available?")
        print(es.cluster.health()['status'])
        str_error = None
    except Exception as str_error:
        pass
    if str_error:
        print("ES not ready... Sleeping for 10 seconds")
        sleep(10)  # wait for 2 seconds before trying to fetch the data again
    else:
        break


  

  print("Deleting index " + INDEX_NAME + "if it exists")
  es.indices.delete(index=INDEX_NAME, ignore=[400, 404])
  print("Create " + INDEX_NAME + " with mappings/settings")
  es.indices.create(index=INDEX_NAME, ignore=400, body=INDEX_SCHEMA)
  bulkLoadLocations(es)

  time.sleep(1)
  print(INDEX_NAME + " count " + str(es.count(index = INDEX_NAME, doc_type = DOC_TYPE)['count']))


if __name__ == "__main__":
    main()