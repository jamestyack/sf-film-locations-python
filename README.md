Docker image that runs python scripts to create/seed Elasticsearch index with open data showing movie locations in SF.

Data source:- https://data.sfgov.org/Culture-and-Recreation/Film-Locations-in-San-Francisco/yitu-d5am/data
